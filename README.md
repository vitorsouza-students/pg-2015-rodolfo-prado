# README #

Projeto de Graduação de Bruno Rodolfo Prado: _Aplicação do método FrameWeb no desenvolvimento de um sistema de informação utilizando o framework VRaptor 4_.

### Resumo ###

Devido à ascensão da Internet como um dos principais meios de comunicação, houve uma demanda por software de qualidade funcionando nesta plataforma. Porém esses programas se tornaram complexos demais para serem desenvolvidos no modo adhoc usado previamente. Assim, foi necessário o uso de conceitos de Engenharia de Software no desenvolvimento de sistemas e aplicações que funcionem na Web.

Foi proposto então o método FrameWeb (Framework-based Design Method for Web En- gineering), que sugere a utilização de uma série de frameworks, juntamente com varias recomendações, que agilizam as principais fases de desenvolvimento. Em sua proposta original foram utilizados apenas um framework de cada categoria, de modo que não é possível saber se o método é aplicável quando uma coleção diferente de frameworks são usados.

Para testar a eficacia do método com diferentes frameworks foi desenvolvido uma WebApp — um Sistema de Controle de Afastamentos de Professores, ou SCAP — com um conjunto diferente de frameworks, baseados na plataforma Java EE 7. O objetivo deste trabalho é reimplementar o SCAP testando o método FrameWeb com o framework controlador frontal VRaptor 4 ao invés dos frameworks utilizados em experimentos anteriores com o
método.

